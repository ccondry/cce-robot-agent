#===================================================================================
# IMPORTANT
#
# This sample is intended for distribution on Cisco Developer Network (CDN). It does
# not form part of a Cisco software product release and is not Cisco TAC supported.
# You should refer to the CDN website for the support rules that apply to samples
# published for download from CDN.
#===================================================================================
#
# ROBOT AGENT EXTEND & CONNECT CALL DESTINATION SIMULATOR
#
# This script can be deployed on voice gateways to act as a bank of dummy
# destinations and robot agents Extend & Connect phones.
#
# The script can be deployed simply as a service without any additional
# configuration parameters on one or more dial-peers as required for the
# dial plan.
#
# The default settings can be overridden by setting the service parameters
# listed below and for the more adventurous TCL developer, it can easily be
# extended to support additional call outcomes.
#
# Service parameters:
#
#	robodest-maxtime-ringback       - Maximum ringback duration before call is
#					  answered.
#
#	robodest-mintime-ringback       - Minimum ringback duration before call is
#					  answered. Maxtime takes precedence over mintime if 
#					  mintime is greater.
#
#	robodest-maxtime-connect	- Maximum connected duration before call is
#					  disconnected.
#
#	robodest-mintime-connect	- Minimum connected duration before call is
#					  disconnected. Maxtime takes precedence
#                     over mintime if mintime is greater.
#
#	robodest-maxtime-greeting       - Maximum duration of the greeting played
#					  when the call is answered. Set to zero if
#					  no greeting is required.
#
#	robodest-mintime-greeting       - Minimum duration of the greeting played
#					  when the call is answered. If the greeting
#					  mintime is set to be greater than maxtime
#					  then maxtime takes precedence.
#
#	robodest-media-greeting		- The media file to play when the call is
#					  answered.  The file is repeated if its
#					  duration is less than greeting maxtime.
#
#	robodest-repeat-greeting       - Repeat the media file. Defaults to true.
#
#	robodest-greeting-delay        - Delay before starting to play greeting,
#	                  in seconds. Default is 1.
#
# History --------------------------------------------------------------------------
#
#	1.0 16-Mar-2013 Paul Tindall	Created to terminate Extend & Connect calls
#	1.1 14-Nov-2014 Coty Condry     Add greeting repeat option and greeting delay
#
#-----------------------------------------------------------------------------------

# Call handling parameters ---------------------------------------------------------

	set MAXTIME_RINGBACK	 20
	set MINTIME_RINGBACK	  3
	set MAXTIME_CONNECT	180
	set MINTIME_CONNECT	 30
	set MAXTIME_GREETING	180
	set MINTIME_GREETING	180
	set MEDIA_GREETING	flash:holdmusic.wav
    set REPEAT_GREETING true
    set GREETING_DELAY 1


# Read parameters to override default timers ---------------------------------------

	if {[infotag get cfg_avpair_exists robodest-maxtime-ringback]} {
            set MAXTIME_RINGBACK [infotag get cfg_avpair robodest-maxtime-ringback]
	}
	puts -nonewline "ROBODEST, Max ringback before answer = $MAXTIME_RINGBACK secs"

	if {[infotag get cfg_avpair_exists robodest-mintime-ringback]} {
            set MINTIME_RINGBACK [infotag get cfg_avpair robodest-mintime-ringback]

            if {$MINTIME_RINGBACK > $MAXTIME_RINGBACK} {
                set MINTIME_RINGBACK $MAXTIME_RINGBACK
            }
	}
	puts -nonewline "ROBODEST, Min ringback before answer = $MINTIME_RINGBACK secs"

	if {[infotag get cfg_avpair_exists robodest-maxtime-connect]} {
            set MAXTIME_CONNECT [infotag get cfg_avpair robodest-maxtime-connect]
	}
	puts -nonewline "ROBODEST, Max connect time = $MAXTIME_CONNECT secs"

	if {[infotag get cfg_avpair_exists robodest-mintime-connect]} {
            set MINTIME_CONNECT [infotag get cfg_avpair robodest-mintime-connect]

            if {$MINTIME_CONNECT > $MAXTIME_CONNECT} {
                set MINTIME_CONNECT $MAXTIME_CONNECT
            }
	}
	puts -nonewline "ROBODEST, Min connect time = $MINTIME_CONNECT secs"

	if {[infotag get cfg_avpair_exists robodest-maxtime-greeting]} {
            set MAXTIME_GREETING [infotag get cfg_avpair robodest-maxtime-greeting]
	}
	puts -nonewline "ROBODEST, Max greeting time = $MAXTIME_GREETING secs"

	if {[infotag get cfg_avpair_exists robodest-mintime-greeting]} {
            set MINTIME_GREETING [infotag get cfg_avpair robodest-mintime-greeting]

            if {$MINTIME_GREETING > $MAXTIME_GREETING} {
                set MINTIME_GREETING $MAXTIME_GREETING
            }
	}
	puts -nonewline "ROBODEST, Min greeting time = $MINTIME_GREETING secs"

	if {[infotag get cfg_avpair_exists robodest-media-greeting]} {
            set MEDIA_GREETING [infotag get cfg_avpair robodest-media-greeting]
	}
	puts -nonewline "ROBODEST, greeting media file = $MEDIA_GREETING"

    if {[infotag get cfg_avpair_exists robodest-repeat-greeting]} {
            set REPEAT_GREETING [infotag get cfg_avpair robodest-repeat-greeting]
	}
	puts -nonewline "ROBODEST, repeat greeting = $REPEAT_GREETING"

    if {[infotag get cfg_avpair_exists robodest-greeting-delay]} {
            set GREETING_DELAY [infotag get cfg_avpair robodest-greeting-delay]
	}
	puts -nonewline "ROBODEST, greeting delay = $GREETING_DELAY second"


#-----------------------------------------------------------------------------------

proc setup { } {

	set cli [infotag get leg_ani]
	set dest [infotag get leg_dnis]
	puts -nonewline "ROBODEST, Incoming call from $cli to $dest, leg [infotag get leg_incoming]"
	ring_then_answer
}


proc ring_then_answer { } {
	global MAXTIME_RINGBACK MINTIME_RINGBACK

	set t [expr {$MAXTIME_RINGBACK - $MINTIME_RINGBACK}]
	set ringsecs [expr {round(rand() * $t) + $MINTIME_RINGBACK}]

        if {$ringsecs > 0} {
            leg alert leg_incoming
            timer start named_timer $ringsecs ringwait
            puts -nonewline "ROBODEST, Ringing for $ringsecs secs on leg [infotag get leg_incoming]"
            fsm setstate RINGBACK
        } else {
            answer
        }
}


proc answer { } {
	global MAXTIME_CONNECT MINTIME_CONNECT GREETING_DELAY

	set t [expr {$MAXTIME_CONNECT - $MINTIME_CONNECT}]
	set connectsecs [expr {round(rand() * $t) + $MINTIME_CONNECT}]
	leg connect leg_incoming
	timer start named_timer $connectsecs connectwait
	puts -nonewline "ROBODEST, Connected for $connectsecs secs max on leg [infotag get leg_incoming]"
	fsm setstate CONNECTED
    timer start named_timer $GREETING_DELAY greetingdelay
}


proc greeting { } {
	global MAXTIME_GREETING MINTIME_GREETING MEDIA_GREETING
    if {$MAXTIME_GREETING > 0} {
            set t [expr {$MAXTIME_GREETING - $MINTIME_GREETING}]
            set greetsecs [expr {round(rand() * $t) + $MINTIME_GREETING}]
            media play leg_incoming $MEDIA_GREETING
            timer start named_timer $greetsecs greetingwait
            puts -nonewline "ROBODEST, Greeting playing for $greetsecs secs max on leg [infotag get leg_incoming]"
            fsm setstate MEDIA_PLAYING
	}
}


proc log_event { } {
	set ev [infotag get evt_event]
	set state [infotag get evt_state_current]
	puts -nonewline "ROBODEST, Event $ev received in state $state"
}


proc connected_timer_expired { } {
	set timer [infotag get evt_timer_name]
	puts -nonewline "ROBODEST, timer <$timer> expired while connected"

	if {[string equal $timer connectwait]} {
            disconnect

	} elseif {[string equal $timer greetingwait]} {
            media stop leg_incoming
            fsm setstate CONNECTED
	} elseif {[string equal $timer greetingdelay]} {
           greeting 
	}

}


proc replay_media { } {
	global MEDIA_GREETING REPEAT_GREETING

	if {$REPEAT_GREETING} {
        media play leg_incoming $MEDIA_GREETING
	    puts -nonewline "ROBODEST, Replaying greeting on leg [infotag get leg_incoming]"
    }
}


proc disconnect { } { 
	leg disconnect leg_incoming
	puts -nonewline "ROBODEST, Disconnected leg [infotag get leg_incoming]"
	fsm setstate DISCONNECTING
}


proc cleanup { } {
   call close
}


requiredversion 2.0

#-----------------------------------------------------------------------------------
#   State Machine
#-----------------------------------------------------------------------------------

set FSM(any_state,ev_any_event)			"log_event,same_state"
set FSM(any_state,ev_disconnected)		"disconnect,same_state"
set FSM(any_state,ev_disconnect_done)		"cleanup,same_state"
set FSM(DISCONNECTING,ev_disconnect_done)	"cleanup,same_state"
set FSM(WAIT_FOR_CALL,ev_setup_indication)	"setup,same_state"
set FSM(RINGBACK,ev_named_timer)		"answer,same_state"
set FSM(CONNECTED,ev_named_timer)		"connected_timer_expired,same_state"
set FSM(MEDIA_PLAYING,ev_named_timer)		"connected_timer_expired,same_state"
set FSM(MEDIA_PLAYING,ev_media_done)		"replay_media,same_state"

fsm define FSM WAIT_FOR_CALL



